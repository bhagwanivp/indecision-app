import React from 'react';

const Action = (props) => (
    <div className='action-wrap'>
        <button className="btn btn-pick-option"
            disabled={ !props.hasOptions }
            onClick={ props.pickOption }
        >What Should I Do?
        </button>
    </div>
);

export default Action;