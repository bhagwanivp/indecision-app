import React from 'react';
import Option from './Option';

const Options = (props) => (
    <div className='options-wrap'>
        <div className='options-title-wrap'>
            <p>Your Options</p>
            <button
                onClick={props.deleteAllOptions}
                className='btn btn-remove-all'
            >Remove all
            </button>
        </div>
        { props.options.length === 0 && <p>Please add an option to get started!</p> }
        {
            props.options.map( (option, index) => 
                <Option
                    key={option}
                    deleteOption={props.deleteOption}
                    count={index + 1}
                    optionText={option}
                />
            )
        }
    </div>
);

export default Options;