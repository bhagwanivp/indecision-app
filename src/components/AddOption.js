import React from 'react';

class AddOption extends React.Component {
    state = {
        error: undefined
    };
    // convert event handler methods to arrow functions for auto binding
    handleAddOption = (e) => {
        e.preventDefault(); // stop default submit method
        const new_option = e.target.elements.option.value.trim(); //remove spaces
        const error = this.props.addOption(new_option); // send option, get error if any
        this.setState( () => ({ error }) ); // display error onscreen
        if(!error) {
            e.target.elements.option.value = ''; // clear input box if option valid
        }
    }
    render() {
        return (
            <div className='add-options-wrap'>
                { this.state.error && 
                    <div className='error-wrap'>
                        <p>{this.state.error}</p>
                    </div> 
                }
                <form onSubmit={this.handleAddOption} >
                    <input type="text" name="option" />
                    <button className='btn btn-add-option'>
                        Add Option
                    </button>
                </form>
            </div>
        );
    }
}

export default AddOption