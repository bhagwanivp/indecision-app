import React from 'react';
import Modal from 'react-modal';

const OptionModal = (props) => (
    <Modal
        className="option-modal"
        isOpen={!!props.selectedOption}
        onRequestClose={props.closeModal}
        contentLabel="Selected Option"
        closeTimeoutMS={100}
    >
        <h2>Selected Option</h2>
        <p>{props.selectedOption}</p>
        <button
            className="btn btn-close-modal"
            onClick={props.closeModal}
        >Okay
        </button>
    </Modal>
);

export default OptionModal;