import React from 'react';

import Header from './Header';
import AddOption from './AddOption';
import Action from './Action';
import Options from './Options';
import OptionModal from './OptionModal';

class IndecisionApp extends React.Component {
    state = {
        options: [],
        selectedOption: undefined
    };

    // methods to let child components update the state of parent
    deleteAllOptions = () => {
        this.setState( () => ({ options: [] }) );
    }

    pickOption = () => {
        const randomNum = Math.floor(Math.random() * this.state.options.length);
        const selected = this.state.options[randomNum];
        this.setState( () => ({ selectedOption: selected }) );
    }

    closeModal = () => {
        this.setState( () => ({ selectedOption: undefined }) );
    }

    addOption = (new_option) => {
        if(!new_option) {
            return 'Enter a valid option';
        } 
        else if(this.state.options.indexOf(new_option) > -1) {
            return 'This option already exists';
        }
        this.setState(
            (prevState) => ({ options: prevState.options.concat(new_option) })
        );
    }

    deleteOption = (old_option) => {
        this.setState(
            (prevState) => ({ 
                options: prevState.options.filter( 
                    (option) => option !== old_option
                )
            })
        );
    }

    // Lifecycle methods
    componentDidMount() {
        try {
            // read user options from local storage and set app state
            const json = localStorage.getItem('user_options');
            const options = JSON.parse(json);
            if(options) { 
                this.setState( () => ({ options }) );
                console.log('fetched user options from local Storage');
            }
            console.log('mounted Indecision App successfully');
        }
        catch(e) {
            // json from the local storage is invalid, so do nothing
        }
    }
    componentDidUpdate(prevProps, prevState) {
        if(prevState.options.length !==  this.state.options.length) {
            const json = JSON.stringify(this.state.options);
            localStorage.setItem("user_options",json);
            console.log('updated app successfully. data saved.');            
        }
    }
    componentWillUnmount() {
        console.log('unmounting Indecision App now');
    }

    render() {
        return (
            <div className='indecision-app-wrap'>
                <Header />
                <div className="container">
                    <Action
                        hasOptions={this.state.options.length > 0}
                        pickOption={this.pickOption}
                    />
                    <Options
                        options={this.state.options}
                        deleteAllOptions={this.deleteAllOptions}
                        deleteOption={this.deleteOption}
                    />
                    <AddOption
                        addOption={this.addOption}
                    />
                </div>
                <OptionModal
                    selectedOption={this.state.selectedOption}
                    closeModal={this.closeModal}
                />
            </div>
        );
    }
}

export default IndecisionApp;