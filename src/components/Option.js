import React from 'react';

const Option = (props) => (
    <div className='option-wrap'>
        <p>{props.count}. {props.optionText}</p>
        <button className="btn btn-remove-option"
            onClick={ (e) => { props.deleteOption(props.optionText) } }
        >Remove
        </button>
    </div>
);

export default Option;