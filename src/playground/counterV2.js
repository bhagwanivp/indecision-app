class Counter extends React.Component {
    constructor(props) {
        super(props);
        this.addOne = this.addOne.bind(this);
        this.minusOne = this.minusOne.bind(this);
        this.resetCount = this.resetCount.bind(this);
        this.state = {
            // count: props.count
            count: 0
        };
    }

    // Lifecycle methods
    componentDidMount() {
        console.log('mounted Counter app successfully');
        let str_count = localStorage.getItem('count');
        let int_count = parseInt(str_count, 10); // string to decimal (base 10) conversion
        if( !isNaN(int_count) ) {
            this.setState( () => ({ count: int_count }) );
            console.log('fetched user count from local storage');
        }
    }
    componentDidUpdate(prevProps, prevState) {
        if(prevState.count !== this.state.count) {
            localStorage.setItem('count',this.state.count);
            console.log('updated app successfully. new count saved: ' + this.state.count);      
        }
    }
    componentWillUnmount() {
        console.log('unmounting Counter App now');
    }

    addOne() {
        // used updater arrow function inside setState, instead of passing 
        // objects directly to avoid stale data. 
        // This happens because React clubs your asynchronous setState 
        // commands in order to avoid unnecessary DOM updates.
        // The arrow function instantly updates state.
        this.setState(
            (prevState) => {
                return { count: prevState.count + 1 };
            }
        );
        // console.log('add one. new count: ' + this.state.count);
    }
    minusOne() {
        this.setState(
            (prevState) => {
                return { count: prevState.count - 1 };
            }
        );
        // console.log('minus one. new count: ' + this.state.count);
    }
    resetCount() {
        this.setState( () => { return { count: 0 }; } );
        // console.log('reset counter to 0');
    }
    render() {
        return(
            <div>
                <h1>Count: {this.state.count}</h1>
                <button onClick={this.addOne} >+1</button>
                <button onClick={this.minusOne} >-1</button>
                <button onClick={this.resetCount} >reset</button>
            </div>
        );
    }
}

// default props for Counter app
// Counter.defaultProps = {
//     count: 0
// };

const appRoot = document.getElementById('app');

ReactDOM.render(<Counter />, appRoot);