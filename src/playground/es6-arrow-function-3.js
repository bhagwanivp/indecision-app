const user = {
    name:'Vivek',
    cities: ['Jabalpur', 'Nagpur', 'Hyderabad', 'Pune'],
    // new syntax, shorthanded arrow function
    printPlacesLived() {
        // map method instead of forEach
        // forEach only lets you use each item in the array
        // map method can transform each item to create new item(s)
        return this.cities.map((city) => this.name + ' has lived in ' + city);
    }
};
// test
console.log(user.printPlacesLived());