// var can be redefined and reassigned
var nameVar = "Vivek";
var nameVar = "Bhagwani";
console.log('nameVar', nameVar);

// let cannot be redefined but can be reassigned
let nameLet = "Jen";
nameLet = "Julie";
console.log('nameLet',nameLet);

// const cannot be redefined or reassigned
const nameConst = "Frank";
console.log('nameConst', nameConst);

// Block Scoping
const fullName = "Vivek Bhagwani";
let firstName;

if(fullName) {
    firstName = fullName.split(" ")[0];
    console.log(firstName);

}
console.log(firstName);