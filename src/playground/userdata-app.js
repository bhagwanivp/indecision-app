// // constants for user data
// const userName = 'Vivek Bhagwani';
// const userAge = 24;
// const userLocation = 'Pune, Maharashtra, India';

// object for user data
const user = {
    name: 'Vivek Bhagwani',
    age: 24,
    location: 'Pune, Maharashtra, India'
};

// if conditions, ternary operator, && operator to be covered next
function getLocation(location) {
    if(location) {return <p>Location: {location}</p>;}
};

// dynamic template for challenge
const template = (
    <div>
        <h1>{user.name ? user.name : 'Anonymous'}</h1>
        {(user.age && user.age >= 18) && <p>Age: {user.age}</p>}
        {getLocation(user.location)}
    </div>
);

const appRoot = document.getElementById('app');

ReactDOM.render(template, appRoot);
