// app template injection path
const appRoot = document.getElementById('app');

// app rendering arrow function
const renderApp = () => {
    const template = (
        <div>
            <h1>Visibility toggle</h1>
            <button onClick={toggleVisibility}>
                {visible?'Hide details':'Show details'}
            </button>
            {visible && (<p>Hello! here are the details you can now see.</p>)}
        </div>
    );

    ReactDOM.render(template, appRoot);
}

let visible = false;

const toggleVisibility = () => {
    visible = !visible;
    // re-render app to update changes
    renderApp();
};

renderApp();