// Challenge for arrow functions, map methods, ES6 new syntax

// multiplier object to be created
const multiplier = {
    // numbers - an array of numbers to be multiplied
    numbers: [1, 2, 3],
    // multiplyBy - a single number that is the multiplier
    multiplyBy: 2,
    // multiply - function using map method to return the multiplied numbers array
    multiply() {
        return this.numbers.map( (n) => n*this.multiplyBy);
    }
}

// test
console.log(multiplier.multiply());