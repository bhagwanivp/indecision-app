let count = 0;

const addOne = () => {
    count++;
    console.log('addOne', count);
    renderCounterApp();
};

// Challenge for Counter app
// make a -1 button - setup minusOne function & register log - 'minusOne'
const minusOne = () => {
    count--;
    console.log('minusOne', count);
    // challenge - subtract one and re-render the app to update count to screen
    renderCounterApp();
};

// make reset button - setup reset function & register log - 'reset'
const reset = () => {
    count = 0;
    console.log('reset', count);
    // challenge - reset to 0 and re-render the app to update count to screen
    renderCounterApp();
};

const templateTwo = (
    <div>
        <h1>Count: {count}</h1>
        <button onClick={addOne}>+1</button>
        <button onClick={minusOne}>-1</button>
        <button onClick={reset}>reset</button>
    </div>
);


const appRoot = document.getElementById('app');

const renderCounterApp = () => {
    const templateTwo = (
        <div>
        <button onClick={addOne}>+1</button>
            <h1>Count: {count}</h1>
            <button onClick={minusOne}>-1</button>
            <button onClick={reset}>reset</button>
        </div>
    );
    
    ReactDOM.render(templateTwo, appRoot);
}

renderCounterApp();