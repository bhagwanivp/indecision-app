// test log to console
console.log("App.js is running!");

// app injection path
const appRoot = document.getElementById('app');

// dynamic template #2 for challenge
const app = {
    title: 'Indecision App',
    subtitle: 'Put your life in the hands of a computer',
    options: []
};

// arrow function for rendering app
const renderIndecisionApp = () => {
    // JSX - JavaScript XML
    const template = (
        <div>
            <h1>{app.title}</h1>
            {app.subtitle && <p>{app.subtitle}</p>}
            <p>{app.options.length ? 'Here are your options' : 'No options'}</p>
            <button disabled={!app.options.length} onClick={makeDecision}>What Should I Do?</button>
            <button disabled={!app.options.length} onClick={removeAll}>Remove All</button>
            <ol>
            {/* map over app.options returning array of li's */
                  app.options.map((option) => <li key={option}>{option}</li>)          
            }
            </ol>
            <form onSubmit={onFormSubmit}>
    
                <input type="text" name="option" />
                <button>Add Option</button>
            </form>
        </div>
    );

    // render app
    ReactDOM.render(template, appRoot);
};

// form update function that runs on clicking the submit button
const onFormSubmit = (e) => {
    e.preventDefault();
    console.log(e.isDefaultPrevented()?'reload prevented':'reload prevention failed');
    const option = e.target.elements.option.value;
    if(option) {
        app.options.push(option);
        e.target.elements.option.value = '';
        console.log('form submitted, option entered: ', option);
        
        // re-render app to update changes
        renderIndecisionApp();
    }
};

const makeDecision = () => {
    const randomNum = Math.floor(Math.random() * app.options.length);
    const selected = app.options[randomNum];
    alert(selected);
    console.log(randomNum);
}

const removeAll = (e) => {
    e.preventDefault();
    app.options = [];
    console.log('removed all options.');
    // re-render app to update changes
    renderIndecisionApp();
}

renderIndecisionApp();