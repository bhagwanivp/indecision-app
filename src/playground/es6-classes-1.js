class Person {
    constructor(name = 'Anonymous', age = 0) {
        // save input arguments into class attributes
        this.name = name;
        this.age = age;
    }
    // challenge
    // set up constructor to take name and age as arguments and default age is 0
    // function getDescription to return '(name) is (age) year(s) old'
    getDescription() {
        return `${this.name} is ${this.age} year(s) old.`;
    }
    getGreeting() {
        // return 'Hi, I am ' + this.name + '!';
        // template string
        return `Hi, I am ${this.name}!`;
    }
}

// const me = new Person('Vivek Bhagwani',24);
// console.log(me.getGreeting(),me.getDescription());

// const other = new Person();
// console.log(other.getGreeting(),other.getDescription());

// subclass
class Student extends Person {
    constructor(name,age,major) {
        // pass arguments to main class constructor
        super(name, age);
        // save other input arguments into class attributes
        this.major = major;
    }

    // exclusive method for subclass
    hasMajor() {
        return !!this.major;
    }

    // override main class function
    getDescription() {
        // fetch response from the main class function
        let description = super.getDescription();
        // append conditional information
        if(this.hasMajor()) {
            description += ` Their major is ${this.major}.`;
        }
        return description;
    }
}

// const me = new Student('Vivek Bhagwani', 24, 'Electronics');
// console.log(me.getDescription());

// const other = new Student();
// console.log(other.getDescription());

// Challenge
// Create subclass of Person called Traveler with new argument homeLocation
// override getGreeting - if homeLocation exists, append 'I'm visiting from (homeLocation)'
class Traveler extends Person {
    constructor(name, age, homeLocation) {
        super(name, age);
        this.homeLocation = homeLocation;
    }

    hasHome() {return !!this.homeLocation;}

    getGreeting() {
        let greeting = super.getGreeting();
        if(this.hasHome()) {
            greeting += `I'm visiting from ${this.homeLocation}.`;
        }
        return greeting;
    }
}

const me = new Traveler('Vivek Bhagwani', 24, 'Nagpur');
console.log(me.getGreeting());

const other = new Traveler(undefined, undefined, 'Nowhere');
console.log(other.getGreeting());