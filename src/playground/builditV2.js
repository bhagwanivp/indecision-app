// visibility toggle app made in Stateful Component structure
class VisibilityToggle extends React.Component {
    constructor(props) {
        super(props);
        this.toggleVisibility = this.toggleVisibility.bind(this);
        this.state = {
            visible: false
        }
    }
    toggleVisibility() {
        this.setState(
            (prevState) => {
                return { visible: !prevState.visible };
            }
        );
        console.log(this.state.visible ? 'details visible': 'details not visible');
    }
    render() {
        return (
            <div>
                <h1>Visibility toggle</h1>
                <button onClick={this.toggleVisibility} >
                    {this.state.visible ? 'Hide details' : 'Show details'}
                </button>
                {this.state.visible && (<p>Hello! here are the details you can now see.</p>)}
            </div>
        );
    }
}

// app template injection path
const appRoot = document.getElementById('app');

// app render call
ReactDOM.render(<VisibilityToggle />, appRoot);