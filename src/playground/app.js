// Constants
// const app = {
//     title: "Indecision",
//     subtitle: "Put your life in the hands of a computer",
// }

// parent app
class IndecisionApp extends React.Component {
    constructor(props) {
        super(props);
        
        // state definition and initialisation
        this.state = {
            // options: props.options   // user can pass options in argument
            options: []
        }

        // binding methods to parent
        this.deleteAllOptions = this.deleteAllOptions.bind(this);
        this.pickOption = this.pickOption.bind(this);
        this.addOption = this.addOption.bind(this);
        this.deleteOption = this.deleteOption.bind(this);
    }
    
    // Lifecycle methods
    // they are not available in SFCs
    componentDidMount() {
        try {
            // read user options from local storage and set app state
            const json = localStorage.getItem('user_options');
            const options = JSON.parse(json);
            if(options) { 
                this.setState( () => ({ options }) );
                console.log('fetched user options from local Storage');
            }
            console.log('mounted Indecision App successfully');
        }
        catch(e) {
            // json from the local storage is invalid, so do nothing
        }
    }
    componentDidUpdate(prevProps, prevState) {
        if(prevState.options.length !==  this.state.options.length) {
            const json = JSON.stringify(this.state.options);
            localStorage.setItem("user_options",json);
            console.log('updated app successfully. saving data');            
        }
    }
    componentWillUnmount() {
        console.log('unmounting Indecision App now');
    }

    // methods to let child components update the state of parent
    deleteAllOptions() {
        // implicit return from setState call
        this.setState( () => ({ options: [] }) );
        
        // explicit return from setState call
        // this.setState(() => {
        //     return {
        //         options: []
        //     };
        // });
        console.log('deleted all options');
    }
    
    pickOption() {
        const randomNum = Math.floor(Math.random() * this.state.options.length);
        const selected = this.state.options[randomNum];
        alert(selected);
    }
    
    // above methods only received data from parent
    // below methods can send data to parent
    addOption(new_option) {
        if(!new_option) {
            return 'Enter a valid option';
        } 
        else if(this.state.options.indexOf(new_option) > -1) {
            return 'This option already exists';
        }
        // implicit return from setState call
        this.setState(
            (prevState) => ({ options: prevState.options.concat(new_option) })
        );

        // explicit return from setState call
        // this.setState((prevState) => {
        //     return {
        //         options: prevState.options.concat(new_option)
        //     };
        // });
        console.log('added new option - ' + new_option);
    }

    deleteOption(old_option) {
        // OptionText from the option has been received as old_option here
        this.setState(
            (prevState) => ({ 
                options: prevState.options.filter( (option) => option !== old_option )
            }) 
        );
        console.log( 'deleted one option ' + old_option );
    }

    render() {
        return (
            <div className='indecision-app-wrap'>
                <Header
                    // title={app.title}
                    // subtitle={app.subtitle}
                />
                <Action
                    hasOptions={this.state.options.length > 0}
                    pickOption={this.pickOption}
                />
                <Options
                    options={this.state.options}
                    deleteAllOptions={this.deleteAllOptions}
                    deleteOption={this.deleteOption}
                />
                <AddOption
                    addOption={this.addOption}
                />
            </div>
        );
    }
}

// defaultProps for IndecisionApp
// IndecisionApp.defaultProps = {
//     options: []
// };

// Components
// SFCs are faster and cleaner, so we should use SFCs over CCs whenever
// state is not required for the component

// Stateless Functional Component for Header
const Header = (props) => {
    return (
        <div className='header-wrap'>
            <h1>{props.title}</h1>
            { props.subtitle && <h2>{props.subtitle}</h2> }
        </div>
    );
};

// default Props for Header
Header.defaultProps = {
    title: 'Indecision',
    subtitle: 'Put your life in the hands of a computer'
};

// Regular Class Component for Header
// class Header extends React.Component {
//     render() {
//         return (
//             <div className='header-wrap'>
//                 <h1>{this.props.title}</h1>
//                 <h2>{this.props.subtitle}</h2>
//             </div>
//         );
//     }
// }


// SFC for Action
const Action = (props) => {
    return (
        <div className='button-wrap'>
            <button
                disabled={ !props.hasOptions }
                onClick={ props.pickOption }
            >What should I do?
            </button>
        </div>
    );
};

// CC for Action
// class Action extends React.Component {
//     render() {
//         return (
//             <div className='button-wrap'>
//                 <button
//                     disabled={ !this.props.hasOptions }
//                     onClick={ this.props.pickOption }
//                 >What should I do?
//                 </button>
//             </div>
//         );
//     }
// }


// SFC for Options
const Options = (props) => {
    return (
        <div className='options-wrap'>
            <button
                onClick={props.deleteAllOptions}
                className='btn-remove-all'
            >Remove all
            </button>
            { props.options.length === 0 && <p>Please add an option to get started!</p> }
            {
                props.options.map( (option) => 
                    <Option
                        key={option}
                        deleteOption={props.deleteOption}
                        optionText={option}
                    />
                )
            }
        </div>
    );
};

// CC for Options
// class Options extends React.Component {
//     render() {
//         return (
//             <div className='options-wrap'>
//                 <button
//                     onClick={this.props.deleteAllOptions}
//                     className='btn-remove-all'
//                 >
//                 Remove all
//                 </button>
//                 {
//                     this.props.options.map(
//                         (option) => <Option key={option} optionText={option} />
//                     )
//                 }
//             </div>
//         );
//     }
// }

// SFC for Option
const Option = (props) => {
    return(
        <div className='option-wrap'>
            {props.optionText}
            <button
                onClick={ (e) => { props.deleteOption(props.optionText) } }
            >Remove
            </button>
        </div>
    );
};

// CC for Option
// class Option extends React.Component {
//     render() {
//         return(
//             <div className='option-wrap'>
//                 {this.props.optionText}
//                 {/* <button onClick={this.props.deleteOption}>Remove</button> */}
//             </div>
//         );
//     }
// }

// CC for AddOption
class AddOption extends React.Component {
    constructor(props) {
        super(props);
        this.handleAddOption = this.handleAddOption.bind(this);
        this.state = {
            error: undefined
        };
    }
    handleAddOption(e) {
        // stop default submission method for form
        e.preventDefault();

        // trim user input to remove invalid data such as spaces
        const new_option = e.target.elements.option.value.trim();

        // pass new option to parent method or send error to screen
        const error = this.props.addOption(new_option);

        // if error exists, set error state
        // implicit return from setState call        
        this.setState( () => ({ error }) );

        // explicit return from setState call        
        // this.setState(() => {
        //     return { error }; //    ES6 shorthand for error: error
        // });

        // clear input box when option is added
        if(!error) {
            e.target.elements.option.value = '';
        }
    }
    render() {
        return (
            <div className='add-options-wrap'>
                { this.state.error && <p>{this.state.error}</p> }
                <form onSubmit={this.handleAddOption} >
                    <input type="text" name="option" />
                    <button className='btn-add-option'>
                        Add Option
                    </button>
                </form>
            </div>
        );
    }
}

// app injection path
const appRoot = document.getElementById('app');

// app render command
ReactDOM.render(<IndecisionApp />, appRoot);