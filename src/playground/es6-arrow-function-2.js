// important points about arrow functions
// argument objects are no longer bound with arrow functions

const add = (a,b) => {
    // console.log(arguments);
    return a+b;
};

console.log(add(55,1,1001));

// the keyword this is also no longer bound with arrow functions

const user = {
    name:'Vivek',
    cities: ['Jabalpur', 'Nagpur', 'Hyderabad', 'Pune'],
    // regular es5 function with workaround for detached 'this' in child
    printPlacesLived: function () {
        console.log(this.name);
        console.log(this.cities);

        // workaround for the detached 'this' in the below anonymous function
        const that = this;

        this.cities.forEach(function(city) {
            console.log(that.name + ' has lived in ' + city);
        });
    },
    
    // arrow function for the same task, 'this' does not bind to the arrow function 
    // 'this' automatically binds to the parent, which is a regular es6 function here
    // if parent was also an arrow function, 'this' would go to the global scope.
    printPlacesLived2: function() {
        this.cities.forEach((city) => {
            console.log(this.name + ' has lived in ' + city);
        });
    }
    // IMP: as per the new ES6 method definition syntax, the : and the function keyword
    // can be removed from the declaration, basically avoiding the keyword, as follows:
    // printPlacedLived2() {
    //      ...
    // }
};
// test
user.printPlacesLived();
user.printPlacesLived2();