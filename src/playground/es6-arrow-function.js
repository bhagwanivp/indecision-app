// // es5 function assigned to a variable square
// const square = function(x) {return x*x;};
// // test
// console.log(square(7));

// // es5 function with its own name
// function square2(x) {return x*x;}
// // test
// console.log(square2(8));

// // arrow function for the same task
// // arrow functions cannot be named, only assigned to variables.
// const squareArrow = (x) => {return x*x;};
// // test
// console.log(squareArrow(9));

// // arrow function in shorthand
// const squareArrow2 = (x) => x*x;
// // test
// console.log(squareArrow2(10));

const getFirstName = (fullName) => {
    return fullName.split(' ')[0];
};
// test
console.log(getFirstName('Omkar Pathak'));

const getFName = (fullName) => fullName.split(' ')[0];
// test
console.log(getFName('Vivek Bhagwani'));    