// import './utils.js'; //file imports work without the .js extension also

// default imports - can be renamed to anything
import sub from './utils'; // renaming unnamed function to sub
import isSenior from './person'; // renaming isSeniorCitizen function to isSenior

// named imports - cannot be renamed
import { square, add } from './utils';
import { isAdult, canDrink } from './person';

import validator from 'validator';

console.log('app is running!');

console.log( square(4) );
console.log( add(4, square(4)) );
console.log( sub(100, 95) ); 

console.log('adult? ' + isAdult(24));
console.log('can drink? ' + canDrink(24));
console.log('senior? ' + isSenior(24));

console.log(validator.isEmail('test@propeluss.com'));