console.log('utils are running!');

// named exports
export const square = (x) => x * x ; // 1st type of export - inline
export const add = (a,b) => a + b;
// const square = (x) => x * x; // 2nd type of export
// const add = (a,b) => a + b;
// export { square, add };

// default exports
// const subtract = (a, b) => a - b;
// export default subtract; // 1st type of export
// export { subtract as default }; // 2nd type of export
export default (a, b) => a - b; // 3rd type of export - unnamed function