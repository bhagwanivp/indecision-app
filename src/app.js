import React from 'react';
import ReactDOM from 'react-dom';
import IndecisionApp from './components/IndecisionApp'; // main app code
import 'normalize.css/normalize.css'; // reset inbuilt CSS of browsers
import './styles/styles.scss'; // styling CSS

const appRoot = document.getElementById('app'); // app injection path
ReactDOM.render(<IndecisionApp />, appRoot); // app render command