A simple ToDo app with a randomised suggestion for your next ToDo.
built using ReactJS, Redux, SASS, Webpack and served with live-server, data stored in your browser's local storage

Setup instructions:
1. Clone the repository and `cd` into the cloned repository folder. 
    Please email on bhagwanivp@rknec.edu if you are unable to clone.
2. `npm install` or `yarn install`
3. `npm start` or `yarn start`